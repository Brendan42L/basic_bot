const Alpaca = require("@alpacahq/alpaca-trade-api");

const alpaca = new Alpaca({
  keyId: `${process.env.KEY_ID}`,
  secretKey:`${process.env.SECRET}`,
  paper: true,
});


module.exports = alpaca;
