const alpaca = require("../utils/alpacaConnect");

const getBars = async (symbol, start, end,) => {
  try {
    let bars = await alpaca.getCryptoBars(
      symbol,
      
      {
        start: start,
        // end: end,
        timeframe: alpaca.newTimeframe(15, alpaca.timeframeUnit.MIN),
        exchanges: "CBSE"
       
      },
     
      alpaca.configuration
    );

    const barset = [];

    for await (let b of bars) {
      barset.push(b);
    }
 
     return barset;
  } catch (e) {
    console.log(e);
  }
};


module.exports = getBars;
