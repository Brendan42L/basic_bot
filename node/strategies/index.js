const https = require("https");
const axios = require("axios");
const rsiIndicator = require("./indicators/rsi");

module.exports = strategyOne = async (data) => {
  const BARS = data;
  let signals;
  const RSI = rsiIndicator(data);


  axios.httpsAgent = new https.Agent({ keepAlive: true });

  axios
    .post(`${process.env.HOST_NAME}/divergence`, {
      bars: BARS,
      rsi: RSI,
    })
    .then((response) => {
      signals = JSON.parse(response.data);

      const regularBull = Object.values(signals.regularBull);
      console.log("Post Request Response From Django:", regularBull);
    })
    .catch((error) => {
      console.log(error);
    });


  return signals;
};
