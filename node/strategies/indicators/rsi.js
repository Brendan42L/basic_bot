const RSI = require("technicalindicators").RSI;

module.exports = rsiIndicator = (data) => {
  const closePrice = data.map((bar) => parseFloat(bar.Close.toFixed(2)));

  var inputRSI = {
    values: closePrice,
    period: 14,
  };

  const rsi = RSI.calculate(inputRSI);

  return rsi;
};
